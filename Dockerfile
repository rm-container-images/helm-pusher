FROM alpine:3.16

ARG UID=1000
ARG GID=1000
ARG UNAME=helm

ARG YQ_VERSION=v4.29.2


# INSTALL BASE
RUN apk update && \
  apk add wget curl git openssl bash grep

# INSTALL HELM

RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq &&\
  chmod +x /usr/bin/yq

# INSTALL HELM
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
  chmod +x get_helm.sh && \
  ./get_helm.sh

RUN addgroup -g $GID $UNAME
RUN adduser -S -u $UID -G $UNAME $UNAME

USER $UNAME

RUN helm plugin install https://github.com/chartmuseum/helm-push


ENTRYPOINT []
CMD [ "/bin/sh" ]